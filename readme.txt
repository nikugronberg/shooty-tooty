Niku Grönberg 2017

selaimessa pelattava multiplayer-peli

Peli pyörii Phaserin päällä ja käyttää socket.io:ta tiedon
välittämiseen clientin ja serverin välillä. Serveri pyörii
nodejs:än päällä ja käyttää porttia 7001.

Pelin saa toimimaan käynnistämällä gameServer.js:än Node.js:llä
ja tämän jälkeen menemällä selaimella osoitteeseen localhost:7001. 
Yhteyden pitäisimuodostua automaattisesti, jollei ilmene jotain ongelmia.

Pelistä on myös aikasempi singleplayer versio, siihen pääsee käsiksi
singleplayer/index.html

gameServer.js pyörittämiseen tarvitsee:
Node.js
ja sen moduulit:
express
socket.io


Kaikki assetit ovat itsetehtyjä.
