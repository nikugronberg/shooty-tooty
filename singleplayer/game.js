"use strict";

let game = new Phaser.Game(800, 600, Phaser.CANVAS, "game", {preload: preload, create: create, update: update});
let player;
let walls;
let w, a, s, d;
let mouse;
let line;

function preload() {
  game.load.image("player", "assets/pc.png");
  game.load.image("ground", "assets/Ground200x200.png");
  game.load.image("wall", "assets/wall10x10.png");
}

function create() {
  game.physics.startSystem(Phaser.Physics.ARCADE);

  game.add.tileSprite(0, 0, game.width, game.height, "ground");

  walls = game.add.group();
  walls.enableBody = true;

  for(let i=0; i< 50; i++) {
    let wall = walls.create(game.world.randomX, game.world.randomY, "wall");
    wall.scale.setTo(2, 2);
    wall.body.immovable = true;
  }

  player = game.add.sprite(200, 300, "player");
  player.scale.setTo(2, 2);
  player.anchor.setTo(0.5, 0.5);
  game.physics.arcade.enable(player);
  player.body.collideWorldBounds = true;

  /* bullet */
  line = game.add.graphics();
  line.lineStyle(10, 0xffd900, 1);
  line.kill();

  /* controls */
  w = game.input.keyboard.addKey(Phaser.Keyboard.W);
  a = game.input.keyboard.addKey(Phaser.Keyboard.A);
  s = game.input.keyboard.addKey(Phaser.Keyboard.S);
  d = game.input.keyboard.addKey(Phaser.Keyboard.D);
  mouse = game.input.mousePointer;
}

function update() {
  let hitWall = game.physics.arcade.collide(player, walls);
  let speed = 200;
  if(Phaser.Keyboard.SPACEBAR.isDown) {
    speed += speed;
  }

  /* player movement*/
  player.body.velocity.x = 0;
  player.body.velocity.y = 0;

  if(a.isDown && d.isDown) {
    player.body.velocity.x = 0;
    player.body.velocity.y = 0;
  } else if(w.isDown && s.isDown) {
    player.body.velocity.x = 0;
    player.body.velocity.y = 0;
  }
  else if(w.isDown && a.isDown) {
    player.body.velocity.x = -speed / Math.sqrt(2);
    player.body.velocity.y = -speed / Math.sqrt(2);
  } else if(w.isDown && d.isDown) {
    player.body.velocity.x = speed / Math.sqrt(2);
    player.body.velocity.y = -speed / Math.sqrt(2);
  } else if(s.isDown && a.isDown) {
    player.body.velocity.x = -speed / Math.sqrt(2);
    player.body.velocity.y = speed / Math.sqrt(2);
  } else if(s.isDown && d.isDown) {
    player.body.velocity.x = speed / Math.sqrt(2);
    player.body.velocity.y = speed / Math.sqrt(2);
  } else if(a.isDown) {
    player.body.velocity.x = -speed;
  } else if(d.isDown) {
    player.body.velocity.x = speed;
  } else if(w.isDown) {
    player.body.velocity.y = -speed;
  } else if(s.isDown) {
    player.body.velocity.y = speed;
  } else {
    player.body.velocity.x = 0;
    player.body.velocity.y = 0;
  }

  /* player rotation */
  player.rotation = game.physics.arcade.angleToPointer(player) + Math.PI;

  /* shooting */
  if(mouse.leftButton.isDown) {
      if(!line.alive) {
        line = drawBullet(player, mouse, line);
    }
  }

  /* check if killed */
}


const drawBullet = (player, mouse, line) => {
  /* wall hit detection, determinates end point for the line */
  let angle = game.physics.arcade.angleToPointer(player);
  let length = Math.sqrt(game.width * game.width + game.height * game.height);
  let checkLine = new Phaser.Line();
  checkLine = checkLine.fromAngle(player.body.center.x, player.body.center.y, angle, length);
  let endPoint = checkLine.end;
  let anotherPoint = checkLine.end;

  /* actually checking the walls */
  for(let i=0; i<walls.count(); i++) {
    anotherPoint = Phaser.Line.intersectionWithRectangle(checkLine, walls.getAt(i));
    if(anotherPoint === null) {
      continue;
    }
    if(anotherPoint.distance(player.body.center) < endPoint.distance(player.body.center)) {
      endPoint = anotherPoint;
    }
  }

  /* creation of the line */
  line.destroy();
  line = game.add.graphics();
  line.lineStyle(3, 0xffd900, 1);
  line.lifespan = 1000;
  line.moveTo(player.body.center.x, player.body.center.y);
  line.lineTo(endPoint.x, endPoint.y);
  if(line.contains(walls)) {
    line.lineStyle(3, 0x00d900, 1);
  }
  return line;
}
