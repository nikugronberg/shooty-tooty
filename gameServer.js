"use strict";

let express = require("express");
let app = express();
let server = require("http").Server(app);

app.get('/',function(req,res){
    res.sendFile(__dirname + '/indexMultiplayer.html');
});

//app.use('/css',express.static(__dirname + '/css'));
app.use('/scripts',express.static(__dirname + '/scripts'));
app.use('/assets',express.static(__dirname + '/assets'));


server.listen(process.env.PORT || 7001); // Listens to port 7000
console.log('Listening on ' + server.address().port);


let playerList = [];
let walls = generateWalls(800, 600, 50);

let Player = function(startX, startY, startAngle, tint, name, kills, deaths) {
  this.x = startX
  this.y = startY
  this.angle = startAngle;
  this.tint = tint;
  this.name = name;
  this.kills = kills;
  this.deaths = deaths;
}

function onNewPlayer(data) {
  let newPlayer = new Player(data.x, data.y, data.angle, data.tint, data.name, data.kills, data.deaths);
  //console.log(newPlayer);
  console.log("created new player with id: " + this.id);
  newPlayer.id = this.id;

  let currentInfo = {
    id: newPlayer.id,
    x: newPlayer.x,
		y: newPlayer.y,
		angle: newPlayer.angle,
    tint: newPlayer.tint,
    name: newPlayer.name,
    kills: newPlayer.kills,
    deaths: newPlayer.deaths,

  }

  for (let i=0; i<playerList.length; i++) {
		let existingPlayer = playerList[i];
		let playerInfo = {
			id: existingPlayer.id,
			x: existingPlayer.x,
			y: existingPlayer.y,
			angle: existingPlayer.angle,
      tint: existingPlayer.tint,
      name: existingPlayer.name,
      kills: existingPlayer.kills,
      deaths: existingPlayer.deaths,
		};
		//console.log("pushing player");
		//send message to the sender-client only
		this.emit("newEnemyPlayer", playerInfo);
	}
  this.broadcast.emit("newEnemyPlayer", currentInfo);
  playerList.push(newPlayer);
  orderedScoreList();
}

function onMovePlayer(data) {
  let movePlayer = findPlayerById(this.id);
  if(!movePlayer) {
    return;
  }
  movePlayer.x = data.x;
  movePlayer.y = data.y;
  movePlayer.angle = data.angle;

  let moveplayerData = {
		id: movePlayer.id,
		x: movePlayer.x,
		y: movePlayer.y,
		angle: movePlayer.angle
	}

  this.broadcast.emit('enemyMove', moveplayerData);
}

function onClientDisconnect() {
  console.log("client disconnected: " + this.id);
  let removePlayer = findPlayerById(this.id);

  if(removePlayer) {
    playerList.splice(playerList.indexOf(removePlayer), 1);
  }
  this.broadcast.emit("removePlayer", {id: this.id});
}

function findPlayerById(id) {
  for(let i=0; i<playerList.length; i++) {
    if(playerList[i].id === id) {
      return playerList[i];
    }
  }
}

function generateWalls(width, height, amount) {
  let list =  [];
  for(let i=0; i<amount; i++) {
    let coordinate = {
      x: Math.floor(Math.random() * width),
      y: Math.floor(Math.random() * height),
    };
    list.push(coordinate);
  }
  return list;
};

function onBullet(data) {
  data.owner = this.id;
  this.broadcast.emit("bullet", data);
}

function onScoreUpdate(id) {
  //console.log(id);
  for(let i=0; i<playerList.length; i++) {
    if(playerList[i].id === id) {
      playerList[i].kills++;
      //console.log(playerList[i]);
    } else if(playerList[i].id === this.id) {
      playerList[i].deaths++;
      //console.log(playerList[i]);
      this.broadcast.emit("blood", {x: playerList[i].x, y: playerList[i].y, tint: playerList[i].tint});
    }
  }
  orderedScoreList();
}

function orderedScoreList() {
  let statList = [];
  for(let i=0; i<playerList.length; i++) {
    let stats = {
      id: playerList[i].id,
      name: playerList[i].name,
      kills: playerList[i].kills,
      deaths: playerList[i].deaths,
    }
    statList.push(stats);
  }
  statList.sort(function(a, b) {
    return b.kills - a.kills;
  });
  io.emit("scoreUpdate", statList);
}


let io = require("socket.io")(server, {});
io.emit("reconnect");


io.sockets.on("connection", function(socket) {
  socket.emit("walls", walls);
  orderedScoreList();
  console.log("socket connected");
  console.log(socket.id);


  socket.on("disconnect", onClientDisconnect);
  socket.on("newPlayer", onNewPlayer);
  socket.on("movePlayer", onMovePlayer);
  socket.on("bullet", onBullet);
  socket.on("scoreUpdate", onScoreUpdate);
});
