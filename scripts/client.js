"use strict";

let socket;
socket = io.connect({autoConnect: false});

let canvasWidth = 800;
let canvasHeight = 600;

let game = new Phaser.Game(canvasWidth, canvasHeight, Phaser.CANVAS, "gameDiv");

let gameProperties = {
  inGame: false
};


let player;
let playerText;
let walls;
let w, a, s, d;
let mouse;
let playerName = "jaakko";
let timer = 1000;
let style = { font: "16px Arial", fill: "#ffffffaa", align: "center" };
let textUnderPlayer = 25; /* pixels */
let playerNameLength = 12;
let stats = {kills: 0, deaths: 0};
let clientId = "";

let textGroup;
let playerGroup;
let bloodGroup;

/* sounds */
let song;
let death = [];
let shoot;
let hit;

let enemies = [];
let bullets = [];

let main = function(game) {

};

$("#startButton").on("click", function() {
  $(this).prop("disabled", true);
  let startx = Math.floor(Math.random() * canvasWidth);
  let starty = Math.floor(Math.random() * canvasHeight);
  let name = $("#playerName").val().substring(0, playerNameLength);
  $("#playerName").val("");
  if(player) {
    socket.close()
    player.destroy();
    playerText.destroy();
    for(let i=0; i<enemies.length; i++) {
      enemies[i].text.destroy();
      enemies[i].player.destroy();
    }
    enemies = [];
    socket.open();
  }
  /* initialize keys after name selection */
  w = game.input.keyboard.addKey(Phaser.Keyboard.W);
  a = game.input.keyboard.addKey(Phaser.Keyboard.A);
  s = game.input.keyboard.addKey(Phaser.Keyboard.S);
  d = game.input.keyboard.addKey(Phaser.Keyboard.D);
  mouse = game.input.mousePointer;
  createPlayer(startx, starty, name);
  gameProperties.inGame = true;

  socket.emit("newPlayer", {x:startx, y:starty, angle:0, tint: player.tint, name: name, kills: 0, deaths: 0});
});

function onSocketConnected() {
  clientId = this.id
  console.log("connected to server: " + clientId);

}

function createPlayer(x, y, name) {
  //console.log("new player");
  player = game.add.sprite(x, y, "player");
  player.scale.setTo(2, 2);
  player.anchor.setTo(0.5, 0.5);
  player.tint = Math.random() * 0xffffff;
  game.physics.arcade.enable(player);
  player.body.collideWorldBounds = true;
  playerGroup.add(player);

  /* player name */
  playerText = game.add.text(player.x, player.y, name, style);
  playerText.anchor.set(0.5);
  textGroup.add(playerText);
}

function onRemovePlayer(data) {
  let removePlayer = findPlayerById(data.id);
  if(!removePlayer) {
    console.log("player not found: " + data.id);
    return;
  }
  removePlayer.player.destroy();
  removePlayer.text.destroy();
  enemies.splice(enemies.indexOf(removePlayer), 1);
}


let remotePlayer = function(id, startx, starty, angle, tint, name, kills, deaths) {
  this.x = startx;
  this.y = starty;
  this.angle = angle;
  this.id = id;
  this.name = name;
  this.kills = kills;
  this.deaths = deaths;
  this.text = game.add.text(this.x, this.y, name, style)
  this.text.anchor.set(0.5);
  textGroup.add(this.text);

  this.player = game.add.sprite(this.x, this.y, "player");
  this.player.scale.setTo(2, 2);
  this.player.anchor.setTo(0.5, 0.5);
  this.player.tint = tint;
  game.physics.arcade.enable(this.player);
  this.player.body.collideWorldBounds = true;
  playerGroup.add(this.player);
}

function onNewPlayer(data) {
  if(!findPlayerById(data.id)) {
    console.log("new player added " + data.id);
    let newEnemy = new remotePlayer(data.id, data.x, data.y, data.angle, data.tint, data.name);
    enemies.push(newEnemy);
  }
}

function onEnemyMove(data) {
  let movePlayer = findPlayerById(data.id);
  if(!movePlayer) {
    return;
  }
  movePlayer.player.x = data.x;
  movePlayer.player.y = data.y;
  movePlayer.player.angle = data.angle;
  movePlayer.text.x = data.x;
  movePlayer.text.y = data.y + textUnderPlayer;
}

function findPlayerById(id) {
  for(let i=0; i<enemies.length; i++) {
    if(enemies[i].id === id) {
      return enemies[i];
    }
  }
  return false;
}

function onWalls(data) {
  walls = game.add.group();
  walls.enableBody = true;
  for(let i=0; i<data.length; i++) {
    let wall = walls.create(data[i].x, data[i].y, "wall");
    wall.scale.setTo(2, 2);
    wall.body.immovable = true;
  }
  game.world.bringToTop(textGroup);
}

function onBullet(data) {
  let fullLine = {
    lineData: data,
    line: drawLine(data),
  }
  bullets.push(fullLine);
}

function checkForHits(player) {
  for(let i=0; i<bullets.length; i++) {
    if(!bullets[i].line.alive) {
      bullets.splice(i, 1);
    } else {
      let checkLine = new Phaser.Line(bullets[i].lineData.startx, bullets[i].lineData.starty, bullets[i].lineData.endx, bullets[i].lineData.endy);
      if(Phaser.Line.intersectsRectangle(checkLine, player)) {
        /* kill */
        socket.emit("scoreUpdate", bullets[i].lineData.owner);

        let myBlood = game.add.sprite(20, 20, "blood");
        myBlood.frame = Math.floor(Math.random()*5);
        myBlood.tint = player.tint;
        myBlood.x = player.body.center.x;
        myBlood.y = player.body.center.y;
        bloodGroup.add(myBlood);

        respawnPlayer(player);
      }
    }
  }
}

function respawnPlayer(player) {
  player.x = Math.floor(Math.random() * canvasWidth);
  player.y = Math.floor(Math.random() * canvasHeight);
}

function onScoreUpdate(list) {
  if(gameProperties.inGame) {
    hit.play();
    death[Math.floor(Math.random() * 6)].play();
  }
  $(".scoreli").remove();
  for(let i=0; i<list.length; i++) {
    let li = "<li class='scoreli' id='" + list[i].id + "'>" + (i+1) + ". " + list[i].name + " " + list[i].kills + " kills " + list[i].deaths + " deaths</li>";
    $("#scores").append(li);
  }

}

function onBlood(data) {
  let blood = game.add.sprite(20, 20, "blood");
  blood.frame = Math.floor(Math.random()*5);
  blood.tint = data.tint;
  blood.x = data.x;
  blood.y = data.y;
  bloodGroup.add(blood);
}

function onReconnect() {
  location.reload();
}


main.prototype = {
  preload: function() {
    game.stage.disableVisibilityChange = true;
    game.load.image("player", "assets/pc.png");
    game.load.image("ground", "assets/Ground200x200.png");
    game.load.image("wall", "assets/wall10x10.png");
    game.load.spritesheet("blood", "assets/blood.png", 20, 20);

    /* sounds */
    game.load.audio("song", "assets/sounds/song.ogg");
    game.load.audio("hit", "assets/sounds/hit.ogg");
    game.load.audio("shoot", "assets/sounds/shoot.ogg");
    game.load.audio("death1", "assets/sounds/death1.ogg");
    game.load.audio("death2", "assets/sounds/death2.ogg");
    game.load.audio("death3", "assets/sounds/death3.ogg");
    game.load.audio("death4", "assets/sounds/death4.ogg");
    game.load.audio("death5", "assets/sounds/death5.ogg");
    game.load.audio("death6", "assets/sounds/death6.ogg");

    game.physics.startSystem(Phaser.Physics.ARCADE);

  },

  create: function() {
    socket.open();
    socket.autoConnect = true;

    console.log("client started");
    socket.on("connect", onSocketConnected);
    socket.on("walls", onWalls);
    socket.on("newEnemyPlayer", onNewPlayer);
    socket.on("enemyMove", onEnemyMove);
    socket.on("removePlayer", onRemovePlayer);
    socket.on("bullet", onBullet);
    socket.on("scoreUpdate", onScoreUpdate);
    socket.on("reconnect", onReconnect);
    socket.on("blood", onBlood);


  //  socket.on("disconnect",onSocketDisconnect);
    socket.on("error", function(error) {
      console.log(error);
    });

    /* background */
    game.add.tileSprite(0, 0, game.width, game.height, "ground");
    /* groups to handle z-order */
    bloodGroup = game.add.group();
    playerGroup = game.add.group();
    textGroup = game.add.group();

    song = game.add.audio("song");
    for(let i=0; i<6; i++) {
      death.push(game.add.audio("death" + (i+1)));
      death[i].volume = 0.2;
      death[i].allowMultiple = true;
    }
    shoot = game.add.audio("shoot");
    hit = game.add.audio("hit");
    song.volume = 0.1;
    shoot.volume = 0.1;
    shoot.allowMultiple = true;
    hit.volume = 0.15;
    hit.allowMultiple = true;
    song.loopFull();


    audioButtons();
  },

  update: function() {
    if(gameProperties.inGame) {
      let hitWall = game.physics.arcade.collide(player, walls);
      let speed = 200;
      if(game.input.keyboard.addKey(Phaser.Keyboard.SHIFT).isDown) {
        speed += speed/2;
      }

      /* player movement*/
      player.body.velocity.x = 0;
      player.body.velocity.y = 0;

      if(a.isDown && d.isDown) {
        player.body.velocity.x = 0;
        player.body.velocity.y = 0;
      } else if(w.isDown && s.isDown) {
        player.body.velocity.x = 0;
        player.body.velocity.y = 0;
      }
      else if(w.isDown && a.isDown) {
        player.body.velocity.x = -speed / Math.sqrt(2);
        player.body.velocity.y = -speed / Math.sqrt(2);
      } else if(w.isDown && d.isDown) {
        player.body.velocity.x = speed / Math.sqrt(2);
        player.body.velocity.y = -speed / Math.sqrt(2);
      } else if(s.isDown && a.isDown) {
        player.body.velocity.x = -speed / Math.sqrt(2);
        player.body.velocity.y = speed / Math.sqrt(2);
      } else if(s.isDown && d.isDown) {
        player.body.velocity.x = speed / Math.sqrt(2);
        player.body.velocity.y = speed / Math.sqrt(2);
      } else if(a.isDown) {
        player.body.velocity.x = -speed;
      } else if(d.isDown) {
        player.body.velocity.x = speed;
      } else if(w.isDown) {
        player.body.velocity.y = -speed;
      } else if(s.isDown) {
        player.body.velocity.y = speed;
      } else {
        player.body.velocity.x = 0;
        player.body.velocity.y = 0;
      }

      /* name follow */
      playerText.x = player.body.center.x;
      playerText.y = player.body.center.y + textUnderPlayer;
      /* player rotation */
      player.rotation = game.physics.arcade.angleToPointer(player) + Math.PI;

      /* shooting */
      if(mouse.leftButton.isDown && timer >= 1000) {
          timer = 0;
          let lineData = calcBullet(player, mouse, player.tint);
          drawLine(lineData);
          socket.emit("bullet", lineData);
      }

      if(timer < 1000) {
        timer += game.time.physicsElapsedMS;
      }

      /* hit detection */
      checkForHits(player);

      /* update player position */
      socket.emit("movePlayer", {x:player.x, y:player.y, angle:player.angle});

    }
  /* buttons will work even when not in game */
  audioButtons();
  }
}

function audioButtons() {
  if($("#muteMusic").prop("checked")) {
    song.mute = true;
  } else {
    song.mute = false;
  }
  if($("#muteSFX").prop("checked")) {
    hit.mute = true;
    shoot.mute = true;
    death.forEach(function(d) {
      d.mute = true;
    });
  } else {
    hit.mute = false;
    shoot.mute = false;
    death.forEach(function(d) {
      d.mute = false;
    });
  }
}


const calcBullet = (player, mouse, tint) => {
  /* wall hit detection, determinates end point for the line */
  let angle = game.physics.arcade.angleToPointer(player);
  let length = Math.sqrt(game.width * game.width + game.height * game.height);
  let checkLine = new Phaser.Line();
  checkLine = checkLine.fromAngle(player.body.center.x, player.body.center.y, angle, length);
  let endPoint = checkLine.end;
  let anotherPoint = checkLine.end;

  /* actually checking the walls */
  for(let i=0; i<walls.count(); i++) {
    anotherPoint = Phaser.Line.intersectionWithRectangle(checkLine, walls.getAt(i));
    if(anotherPoint === null) {
      continue;
    }
    if(anotherPoint.distance(player.body.center) < endPoint.distance(player.body.center)) {
      endPoint = anotherPoint;
    }
  }

  let lineData = {
    startx: player.body.center.x,
    starty: player.body.center.y,
    endx: endPoint.x,
    endy: endPoint.y,
    tint: tint,
    owner: null,
  };
  return lineData;
}

function drawLine(lineData) {
  shoot.play();
  let newLine = game.add.graphics();
  newLine.lineStyle(3, lineData.tint, 1);
  newLine.lifespan = 1000;
  newLine.moveTo(lineData.startx, lineData.starty);
  newLine.lineTo(lineData.endx, lineData.endy);
  return newLine;
}

let gameBootstrapper = {
    init: function(gameContainerElementId){
		game.state.add('main', main);
		game.state.start('main');
    }
};;

gameBootstrapper.init("gameDiv");
